# custom files: easyminer.keytab, bitbucket-private-key
# building: docker build -t easyminer-backend --build-arg EM_USER_ENDPOINT=https://easyminer-demo.lmcloud.vse.cz/easyminercenter .
# run background  mode: docker run -d -p 8893 -p 8891 -p 8892 --name easyminer-backend easyminer-backend
# run interactive mode: docker run -t -i -p 8893 -p 8891 -p 8892 --name easyminer-backend easyminer-backend -bash
# ports: 8893 - easyminer-miner
#        8891 - easyminer-data
#        8892 - easyminer-preprocessing 
FROM debian:jessie

MAINTAINER kizi "prozeman@gmail.com"

ARG EM_USER_ENDPOINT

WORKDIR /root

# set repositories
RUN echo 'deb http://http.debian.net/debian jessie-backports main' >> /etc/apt/sources.list && \
    echo 'deb http://dl.bintray.com/sbt/debian /' >> /etc/apt/sources.list && \
    echo 'deb http://cran.r-project.org/bin/linux/debian jessie-cran3/' >> /etc/apt/sources.list && \
    echo 'deb [arch=amd64] http://scientific.zcu.cz/repos/hadoop/cdh5/debian/wheezy/amd64/cdh wheezy-cdh5 contrib' >> /etc/apt/sources.list && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823 && \
    apt-key adv --keyserver keys.gnupg.net --recv-key 381BA480 && \
    apt-key adv --fetch-key http://scientific.zcu.cz/repos/hadoop/archive.key && \
    apt-get update

# install tools needed for compilation of the easyminer backend
RUN apt-get install -y openjdk-8-jdk git sbt curl

# install R environment
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ADD r-install.R /root
RUN apt-get install -y r-base r-base-dev libcurl4-openssl-dev libssl-dev && \
    R CMD javareconf && \ 
    Rscript r-install.R

# install hadoop environment
ADD etc/krb5.conf /etc
ADD easyminer.keytab /root
RUN apt-get install -y krb5-admin-server krb5-kdc
RUN apt-get install -y hadoop-client hive spark-python
ADD etc/hadoop/conf/* /etc/hadoop/conf/
ADD etc/hive/conf/* /etc/hive/conf/
ADD etc/spark/conf/* /etc/spark/conf/
ADD etc/profile.d/* /etc/profile.d/
ENV KRB5CCNAME FILE:/tmp/krb5cc_53826
ENV HADOOP_CONF_DIR /etc/hadoop/conf
ENV YARN_CONF_DIR /etc/hadoop/conf
ENV LD_LIBRARY_PATH "/usr/lib/hadoop/lib/native:$LD_LIBRARY_PATH"
RUN chmod 755 /etc/profile.d/*

# set private key for bitbucket
RUN mkdir /root/.ssh/
ADD bitbucket-private-key /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa && \
    touch /root/.ssh/known_hosts && \
    ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# pull easyminer backend code from bitbucket
RUN git clone -b v2.0 git@bitbucket.org:easyminer/easyminer-root.git
WORKDIR easyminer-root
RUN git clone -b v2.0 git@bitbucket.org:easyminer/easyminer-core.git EasyMiner-Core && \
    git clone -b v2.0 git@bitbucket.org:easyminer/easyminer-data.git EasyMiner-Data && \
    git clone -b v2.0 git@bitbucket.org:easyminer/easyminer-preprocessing.git EasyMiner-Preprocessing && \
    git clone -b v2.0 git@bitbucket.org:easyminer/easyminer-miner.git EasyMiner-Miner

# compile easyminer backend to jar files
RUN sbt "project data" "pack" && \
    sbt "project preprocessing" "pack" && \
    sbt "project miner" "pack"

# copy all setting files and compiled files to easyminer service directories
ADD easyminer-data /root/easyminer-data
ADD easyminer-preprocessing /root/easyminer-preprocessing
ADD easyminer-miner /root/easyminer-miner
ADD etc/init.d/* /etc/init.d/
RUN mv EasyMiner-Data/target/pack/lib /root/easyminer-data && \
    mv EasyMiner-Preprocessing/target/pack/lib /root/easyminer-preprocessing && \
    mv EasyMiner-Miner/target/pack/lib /root/easyminer-miner
WORKDIR /root 
RUN mv easyminer-preprocessing/lib/preprocessing_2.11-1.0.jar easyminer-preprocessing/bin && \
    mv easyminer-miner/lib/miner_2.11-1.0.jar easyminer-miner/bin && \ 
    chmod 755 /etc/init.d/easyminer-data && \
    chmod 755 /etc/init.d/easyminer-preprocessing && \
    chmod 755 /etc/init.d/easyminer-miner && \
    chmod 755 easyminer-data/run && \
    chmod 755 easyminer-data/stop && \
    chmod 755 easyminer-data/bin/main && \
    chmod 755 easyminer-preprocessing/run && \
    chmod 755 easyminer-preprocessing/stop && \
    chmod 755 easyminer-preprocessing/bin/main && \
    chmod 755 easyminer-miner/run && \
    chmod 755 easyminer-miner/check && \
    chmod 755 easyminer-miner/bin/main

ENV EM_USER_ENDPOINT $EM_USER_ENDPOINT

ADD start.sh /root
RUN chmod 755 start.sh

EXPOSE 8893 8891 8892
ENTRYPOINT ["./start.sh"]
CMD ["-d"]
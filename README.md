# EasyMiner-Docker #

This is an installation package of the easyminer backend for a docker environment. For the building a docker image, please follow these instructions:

1. First clone this installation sources from bitbucket to your local folder 'easyminer-docker'. This directory should be placed in a platform where a docker environment is installed.
2. Then you need to put some "secret" files to this directory. The first required file is a private key for your bitbucket account which has access to easyminer repositories. Put this private key to the 'easyminer-docker' directory with name 'bitbucket-private-key'. The next required file is a kerberos keytab file with which you are able to access to the easyminer hadoop environment. If you are able to generate a kerberos ticket by kinit, then you can also generate a keytab file by ktutil. Don't forget to put the keytab file to the 'easyminer-docker' folder as 'easyminer.keytab'.
3. Start the build script by this command:
   ```cd easyminer-docker```
   ```
   docker build -t easyminer-backend --build-arg EM_USER_ENDPOINT=<easyminercenter-url> .
   ```
   Where <easyminercenter-url> is a valid URL to the easyminercenter service. All backend services uses this endpoint, therefore you need to install easyminercenter first.
4. After the image has been successfully built you can run it: ```docker run -d -p 8890:8890 -p 8891:8891 -p 8892:8892 --name easyminer-backend easyminer-backend```
5. Finally, you can use all three easyminer backend services: data (exposed port 8891), preprocessing (exposed port 8892) and miner (exposed port 8890)

## Additional information ##

REST API endpoints are accessible on:

* http://localhost:8891/easyminer-data/index.html - data service
* http://localhost:8892/easyminer-preprocessing/index.html - preprocessing service
* http://localhost:8890/easyminer-miner/index.html - miner service

Possible docker RUN modes:

* ```docker run -d easyminer-backend``` - background
* ```docker run -i -t easyminer-backend -bash``` - foreground with bash in stdin

Required environment variables:

* ```EM_USER_ENDPOINT``` - URL to the easyminercenter endpoint

Required custom "secret" files placed in the installation folder:

* ```bitbucket-private-key``` - in this file there is a private key for the access to your bitbucket account. Your account also needs to have access rights to easyminer repositories
* ```easyminer.keytab``` - a kerberos keytab file. You should be able to access to the easyminer hadoop environment with this file which can generate a valid kerberos ticket.